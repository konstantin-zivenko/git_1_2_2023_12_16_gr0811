from numbers import Number


def square(arg: Number) -> float:
    return arg * arg
